# Proxmox Labb

Proxmox Virtual Environment, or [Proxmox VE](https://www.proxmox.com/en/proxmox-ve) is a [type-1 hypervisor](https://en.wikipedia.org/wiki/Hypervisor#Classification). You can think of it as "VirtualBox as a distro".

It is based on Debian, and allows us to create, run and manage VM and LXC Containers via either the CLI, a web gui or an API.

Today we'll use the webgui to setup a VM, and then we'll configure and operate the VM.


# Credentials

You will only be able to reach the proxmox webgui if you're connected to the school's wifi network.

Visit [https://10.126.2.129:8006](https://10.126.2.129:8006) to access the webgui.

The webgui's TLS certificate is self-signed, so you will need to accept the exception in your browser to continue.

You can login with user `doe21` (the password is the same as the username). Make sure to choose VE authentication and not PAM.


# The proxmox cluster

We have 4 servers running proxmox clustered together, you can see a list of them on the left side of the webgui.

You can choose whichever host that's available to deploy your VM to.

If the resources aren't enough to run 1 VM per student, we'll work in groups.


# The LAB

This lab is meant to give you a taste of `ops`, proxmox being a mock of a cloud provider.

This LAB is divided into 2 parts, you should work in pairs as follows:

-   student A and B decide to work together on the LAB
-   in the first half of the lab student A will take care of the first part of the lab, and student B of the second part. The two VMs should work together.
-   once done, A and B switch; so that B fixes the first part of the LAB and A the second (when switching, stop/delete the old VMs if the cluster is low on resources)


## DISCLAIMER

A very important part of this LAB is to let you work on your own, and to train your searching and reading skills together with your favourite search engine.

You'll be given a description of the goal for the lab, but no step-by-step guide on how to achieve the goals.

Keep the hardware specs for the VM as they are (1 core, 1GB ram) to allow for enough VMs for everyone.

Do **NOT** use Docker for any of the service, install everything directly on the VM.


# LAB Part 1

Goals for part 1:

-   Set up a VM (full-clone VM 9000, and use cloud-init to setup the inital credentials for login and networking. Be sure to provide also an ssh-pubkey).
-   Update the system to the latest packages available. Make also sure you have an editor you're familiar with installed in the VM.
-   Set up a firewall (suggestion: `ufw` or `firewalld`) and block all incoming traffic on all ports
-   Setup an `ssh` server listening on port `2222`. Make sure that it is only possible to login with an ssh key and not with a password. Make sure only your user is allowed to log in via ssh. (use `openssh` and not `dropbear`). Open port `2222` in the firewall.
-   Setup a wordpress instance behind a reverse proxy, served on https default port with a self-signed certificate (tip: use either `Caddy` or `Nginx`. If you use nginx you'll have to generate the certificate yourself). Make sure to open the https port on the firewall.
-   Setup zabbix-agent2, so that it will accept connections from the server from Part 2 (ask whoever you're working with the address for their VM). Make sure to open the zabbix-agent port on the firewall.


# LAB Part 2

-   Set up a VM (full-clone VM 9000, and use cloud-init to setup the inital credentials for login and networking. Be sure to provide also an ssh-pubkey).
-   Update the system to the latest packages available. Make also sure you have an editor you're familiar with installed in the VM.
-   Set up a firewall (suggestion: ufw or firewalld) and block all incoming traffic on all ports
-   Setup an ssh server listening on port 2222. Make sure that it is only possible to login with an ssh key and not with a password. Make sure only your user is allowed to log in via ssh. (use openssh and not dropbear). Open port 2222 in the firewall.
-   Setup Zabbix server. Make sure to open all the needed ports in the firewall.
-   Setup monitoring for the host from Part 1. Use the Linux template, and set up even a web scenario to make sure wordpress and the admin panel for wordpress are up and reachable.
-   Setup a MailHog server to receive mail from zabbix server. Setup the media type in Zabbix and the right actions and triggers so you get a mail for all alarms that come up in Zabbix.